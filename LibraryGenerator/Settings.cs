﻿namespace LibraryGenerator
{
    class Settings
    {
        public string   ShortcutDirectory  { get; set; } = "";
        public string[] GameDirectories    { get; set; } = new string[0];
        public string[] ExcludeDirectories { get; set; } = new string[0];
    }
}