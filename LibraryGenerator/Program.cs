﻿using System;
using System.Linq;
using System.Text.Json;
using LibraryGenerator.Services;
using File = System.IO.File;

namespace LibraryGenerator
{
    class Program
    {
        static string _settingsPath;
        static string _errorPath;

        static SettingService _settingService;
        static FolderService _folderService;
        static ShortcutService _shortcutService;

        static void Main()
        {
            try
            {
                Initialize();

                var gameDirs = _folderService.GetGameDirectories();
                var shortcuts = _shortcutService.CreateShortcuts(gameDirs).ToList();

                _settingService.Save(_settingsPath);

                if (File.Exists(_errorPath))
                    File.Delete(_errorPath);
            }
            catch (Exception ex)
            {
                var options = new JsonSerializerOptions { WriteIndented = true };
                var json = JsonSerializer.Serialize(ex, options);

                File.WriteAllText(_errorPath, json);
            }
        }

        static void Initialize()
        {
            _settingsPath = Environment.CurrentDirectory + "\\LibraryGenerator.Settings.json";
            _errorPath = _settingsPath.Replace(".Settings.", ".Error.");

            _settingService = new SettingService(_settingsPath);
            _folderService = new FolderService(_settingService.Settings);
            _shortcutService = new ShortcutService(_settingService.Settings);
        }
    }
}
