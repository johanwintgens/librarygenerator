﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LibraryGenerator.Services
{
    class FolderService
    {
        readonly Settings _settings;

        public FolderService(Settings settings)
        {
            _settings = settings;
        }

        public List<string> GetGameDirectories()
        {
            var scanDirs = _settings.GameDirectories;
            var excludeDirs = _settings.ExcludeDirectories;

            var dirs = new List<string>();

            foreach (var scanDir in scanDirs)
                dirs.AddRange(Directory.GetDirectories(scanDir));

            dirs = dirs.Where(dir => !excludeDirs.Contains(dir)).ToList();

            return dirs;
        }
    }
}