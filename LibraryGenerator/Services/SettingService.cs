﻿using System.IO;
using System.Text.Json;

namespace LibraryGenerator.Services
{
    class SettingService
    {
        public Settings Settings { get; private set; }

        public SettingService(string path)
        {
            LoadSettings(path);
        }

        public void Save(string path, bool indented = true)
        {
            var options = new JsonSerializerOptions { WriteIndented = indented };
            var json    = JsonSerializer.Serialize(Settings, options);

            File.WriteAllText(path, json);
        }

        void LoadSettings(string path)
        {
            if (!File.Exists(path))
            {
                Generate(path);
                return;
            }

            Settings = JsonSerializer.Deserialize<Settings>(File.ReadAllText(path));

            Settings.ShortcutDirectory = Settings.ShortcutDirectory.Replace('/', '\\').Trim('\\');

            for (var i = 0; i < Settings.GameDirectories.Length; i++)
                Settings.GameDirectories[i] = Settings.GameDirectories[i].Replace('/', '\\').Trim('\\');

            for (var i = 0; i < Settings.ExcludeDirectories.Length; i++)
                Settings.ExcludeDirectories[i] = Settings.ExcludeDirectories[i].Replace('/', '\\').Trim('\\');
        }

        void Generate(string path)
        {
            Settings = new Settings();

            Save(path);
        }
    }
}