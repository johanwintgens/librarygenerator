﻿using System.Collections.Generic;
using IWshRuntimeLibrary;

namespace LibraryGenerator.Services
{
    class ShortcutService
    {
        readonly Settings _settings;

        public ShortcutService(Settings settings)
        {
            _settings = settings;
        }

        public IEnumerable<IWshShortcut> CreateShortcuts(List<string> gameDirs)
        {
            foreach (var directory in gameDirs)
            {
                yield return CreateShortcut(directory);
            }
        }

        IWshShortcut CreateShortcut(string target)
        {
            var directoryName = target.Substring(target.LastIndexOf('\\') + 1);

            var shortcut = (IWshShortcut)new WshShell().CreateShortcut(_settings.ShortcutDirectory + $"\\{directoryName}.lnk");

            shortcut.TargetPath       = target;
            shortcut.WindowStyle      = 1;
            shortcut.Description      = directoryName;
            shortcut.WorkingDirectory = target.Substring(0, target.LastIndexOf('\\'));

            //shortcut.Arguments = "";
            //shortcut.IconLocation = "specify icon location";

            shortcut.Save();

            return shortcut;
        }
    }
}